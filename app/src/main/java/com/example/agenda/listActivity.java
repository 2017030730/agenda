package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class listActivity extends AppCompatActivity {
    TableLayout tblLista;
    ArrayList<Contacto> contactos;
    private ArrayList<Contacto> filtro;

    public void cargarContactos()
    {
        for(int x=0; x < contactos.size();x++)
        {
            Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(listActivity.this);
            TextView nText = new TextView(listActivity.this);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor(c.isFavorito()? Color.BLUE:Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(listActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c = (Contacto)v.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto",c);
                    oBundle.putInt("index",Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index, x);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout)findViewById(R.id.tblLista);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>)bundleObject.getSerializable("contactos");
        filtro = contactos;
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview_main, menu);
        MenuItem item = menu.findItem(R.id.search_boton);
        SearchView searchview = (SearchView) item.getActionView();
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    private void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for (int x=0;x<filtro.size(); x++)
        {
            if(filtro.get(x).getNombre().contains(s))list.add(filtro.get(x));
        }
        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }
}
